let
  unstableTarball = fetchTarball https://github.com/NixOS/nixpkgs/archive/nixos-unstable.tar.gz;
  unstable = import unstableTarball { };

  shell = unstable.mkShell {
    buildInputs = with unstable; [
      bash
      slides
      tmux
      go
      python3
    ];
  };

in
shell

