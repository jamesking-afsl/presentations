# Presentations

This is just a group project for holding all the little presentations for things I've built up.

> All presentations are just markdown files, so they can be adapted to any presentation format required by other people.

## How to Present

This project uses [slides](https://github.com/maaslalani/slides) to run presentations.

### Basic Commands

 - `n`, `→`, `l` - Next Slide
 - `p`, `←`, `h` - Previous Slide
 - `/` - Find in Slides
 - `ctrl + e` - Execute commands in Slide
 - `q` - Quit

For a _much_ more extensive list of commands, see the slides [README](https://github.com/maaslalani/slides)


