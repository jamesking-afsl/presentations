# Welcome to Go vs Python for DevOps
I'm not going to jump on a high horse for this!

This is intended to be a very high-level presentation on the benefits and drawbacks of Golang and Python for various scenario's we come across in the DevOps space.

---
# Why Go?

Why not? In my mind, there are two key contributing factors to utilising Go in the DevOps space.

 1. Kubernetes is written in Go - all of it's api orchestration is in Go (kubectl, kustomize etc). Personally I'm a big advocate for using the language / system you work in. 
 2. Our Engineer Team is predominantly Go engineers - this gives us a bigger net to cast for support and maintenance.

### Is this a good enough reason to use Go for everything? 
Absolutely not. There are some things Go is good at, and some that it is not.

One of it's main drawbacks from a DevOps perspective is that it's compiled, not interpreted. Meaning every change to the code means a re-build in order to check your change works.

For now, I am also the only Go engineer in DevOps - so without reaching outside of the team, maintenance could be an issue.

### What does Hello World look like in Go?

As an example of the differences, i'll give a little example of Go code as a reference for what it looks like.

```
package main

import "fmt"

func main() {
    fmt.Println("Hello World")
}
```

### Demo of executing a go program

```bash
tmux display-popup -E "cd $(pwd) && nvim go-vs-python/examples"
```

Here is an example of building a go binary that could be ported over to anything and run standalone.

This will run on MacOS, Linux, Windows, Android...
```
# -o = build artifact name
go build -o example main.go
```

---
# Why Python?

Again, why not? There are a lot of benefits to using python for DevOps tooling. 

The biggest one, by far, is speed of development. Python as a language is interpreted, and is very readable (if written well...) so changes can be observed immediately.

Python is very popular for quick scripts for this reason. On Mac's - python is also "pre installed", all be it incredibly out of date natively.

### Is this a good enough reason to use Python for everything?
Nope. Much like Go, it's about using the right tool for the job.

One of python's biggest drawbacks is that you are heavily reliant of the users environment being correct for your python scripts ro function. There's sometimes a bunch of dependencies a python service / cli may require, not least of which is the correct _version_ of python itself.

### What does Hello World look like in Python?

As an example of the differences, i'll give a little example of python code as a reference to what it looks like.

```
print("Hello World")
```

### Demo of executing a python script
```bash
tmux display-popup -E "cd $(pwd) && nvim go-vs-python/examples"
```
