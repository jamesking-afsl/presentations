# System Management

Probably the most common package manager for Mac is [Homebrew](https://brew.sh/). It's the most common for good reason - it's simple.

Simple is often a good enough reason on it's own to use something, but simple always has drawbacks. 

Cars are fantastic, most people can learn to drive, without knowing hows cars actually work. Why then, are car accidents so common?

People make mistakes, and the simpler the UX, the more innocent and hard to spot those mistakes can be. 

> "A little learning is a dangerous thing" - Alexander Pope

---
# Drawbacks & Pitfalls

There are some common (and very valid) concerns around all managers.

Probably the **most** worrying

## Supply Chain attacks

Every manager should use this as one of it's core pillars during design, if it doesn't - stay very far away.

Things like pinning sha's and hashing package contents, with hash checks to ensure they're not tampered with should be common-place.

## Stability

Every mnager should be putting a focus on stability of their services they're providing. In a lot of cases, this is just packages, but not always.


